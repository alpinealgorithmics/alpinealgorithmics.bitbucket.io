// Expand terminals:

Sk.hardInterrupt = false;
var running = false;

// keyboard interrupt
Sk.builtin.KeyboardInterrupt = function (args) {
    var o;
    if (!(this instanceof Sk.builtin.KeyboardInterrupt)) {
        o = Object.create(Sk.builtin.KeyboardInterrupt.prototype);
        o.constructor.apply(o, arguments);
        return o;
    }
    Sk.builtin.BaseException.apply(this, arguments);
};
Sk.abstr.setUpInheritance("KeyboardInterrupt", Sk.builtin.KeyboardInterrupt, Sk.builtin.BaseException);

// put small sleep on every python line so that the interpreter can
// catch up and display output as it runs
function sleepify(prog_str) {
    prog_str = 'import time\n' + prog_str

    var lines = prog_str.split('\n');

    lines.forEach(function(x,i,a){
	// check for control flow line, e.g. for i in range(10):
	// (or an unfortunate comment ending with : and whitespace)
	// AND check for empty lines
	
	// if neither, add a brief sleep for the output to catch up:
	if ( ! /.*:[ \t]*/.test(a[i]) && a[i].replace(/[ \t]*/,'') )
	    a[i] += '; time.sleep(0.005)'
    });

    var modified_prog = lines.join('\n');
    
    return modified_prog;
}

$("div.terminal").each(function(index){
    var src_url = this.getAttribute('data-src');

    this.innerHTML = "<div class='pair'>"+
	"<div class='code-editor first'><pre></pre>"+
	"</div>"+
	"<div class='button-and-output second'>"+
	"<button type='button' class='button'>&#9889;</button>"+
	"<button type='button' class='button'>&#11035;</button>"+
	"<button type='button' class='button'>&#9883;</button>"+
	"<pre class='output'></pre>"+
	"</div>"+
	"</div>"+
	"</div>";

    // [0] to strip jquery object to DOM node:
    var code_pre = $(this).find(".code-editor > pre")[0];

    // make editor:
    var code_editor = ace.edit(code_pre);
    code_editor.getSession().setUseSoftTabs(true);
    code_editor.getSession().setTabSize(2);
    code_editor.setOption("showPrintMargin", false)
    
    ace.config.set('basePath', './js/');
    code_editor.setTheme("ace/theme/xcode");
    //code_editor.setTheme("ace/theme/monokai");
    code_editor.session.setMode("ace/mode/python");

    function load_code_from_py_file() {
	// insert source file into editor:
	$.get(src_url, function(source_text) {
	    code_editor.setValue(source_text);
	    code_editor.clearSelection();
	});
    }
    
    // cached changes from the user
    var code_hash = 'code'+index;
    if (window.localStorage[code_hash]) {
	code_editor.setValue(window.localStorage[code_hash]);
	code_editor.clearSelection();
    }
    else
	load_code_from_py_file();

    code_editor.getSession().on('change', function(){
	window.localStorage[code_hash] = code_editor.getValue();
    });


    // bind button to run Skulpt on editor text:
    var output = $(this).find('pre.output')[0];

    function scroll_to_bottom() {
	output.scrollTop = output.scrollHeight - output.clientHeight;
    }
    scroll_to_bottom();

    var button_run = $($(this).find('button')[0]);
    var button_stop = $($(this).find('button')[1]);
    var button_revert = $($(this).find('button')[2]);

    function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
    }
    
    function print(text) {
	var is_scrolled_to_bottom = output.scrollHeight - output.clientHeight <= output.scrollTop+1;

	$(output).html( $(output).html() + text );

	if (is_scrolled_to_bottom)
	    scroll_to_bottom();
    }

    function printerr(text) {
	var error_on_line_number = -1;
	var new_text = text.replace(/([1-9][0-9]*)$/, function(x){error_on_line_number = x-1; return error_on_line_number;});
	if (error_on_line_number != -1) {
	    console.log('error with new line number (line number on sleepified code):', text);
	    console.log('error with original line number:', new_text);
	    code_editor.gotoLine(error_on_line_number);
	    $(output).html( $(output).html() + new_text );
	}
	else
	    console.log(text);
    }

    function interruptHandler(susp) {
        if (Sk.hardInterrupt === true) {
	    throw new Sk.builtin.KeyboardInterrupt('aborted execution');
        } else {
	    return null; // should perform default action
        }
    };

    function builtinRead(x) {
        if (Sk.builtinFiles === undefined || Sk.builtinFiles["files"][x] === undefined) throw "File not found: '" + x + "'";
        return Sk.builtinFiles["files"][x];
    }

    var show_hide_speed = 50;

    function buttons_while_running() {
	button_run.hide(show_hide_speed);
	button_stop.show(show_hide_speed);
    }

    function buttons_while_stopped() {
	running = false;

	button_run.show(show_hide_speed);
	button_stop.hide(show_hide_speed);
	// unbind stop button (pressing when not running can cause trouble)
	button_stop.off('click');
    }

    function until(conditionFunction) {
	const poll = resolve => {
	    if(conditionFunction()) resolve();
	    else setTimeout(_ => poll(resolve), 100);
	}

	return new Promise(poll);
    }

    async function execute_code() {
	// if running, stop
	if (running)
	    stop_code();
	
	await until(_ => ! Sk.hardInterrupt);

	running = true;

	buttons_while_running();

	// clear output
        $(output).html('');

        //Sk.canvas = "canvas";
        Sk.pre = "output";
        Sk.configure({
	    output: print,
	    read: builtinRead,
	    // currently used for python3 print, / support
	    __future__: Sk.python3
        });

	// stop button stops skulpt
	button_stop.click(function(){
	    console.log('stop code button clicked');
     	    stop_code();
	});

	// necessary for async (e.g., sleep)
	var promise = Sk.misceval.asyncToPromise(function() {
	    return Sk.importMainWithBody("<stdin>", false, sleepify( code_editor.getValue() ), true);
	}, {"*": interruptHandler});

	promise.then(function(mod) {
	    console.log('success');
	    printerr('PYTHON SCRIPT TERMINATED SUCCESSFULLY');

	    buttons_while_stopped();
	},
		     function(err) {
			 printerr( err.toString() );
			 Sk.hardInterrupt = false;
			 buttons_while_stopped();
		     });
    }

    function stop_code() {
	console.log('stopping');
	
	Sk.hardInterrupt = true;
    }

    // click button = run code
    button_run.click(function(){
	execute_code();
    });

    // shift+enter = run code
    $(this).find('.code-editor').on('keypress', function(e) {
	if (e.keyCode == 13 && e.shiftKey) {
	    e.preventDefault();
	    execute_code();
	}
    });

    // clear button clears output
    button_revert.click(function(){
	load_code_from_py_file();
    });
});

