/*
Aren't you enterprising, looking at the guts of this webpage? : )
Have you considered looking through the Open Positions?
*/

// Set up spoilers:
var spoilers = document.querySelectorAll('.spoiler');
for (var i=0; i<spoilers.length; ++i) {
  var fader = spoilers[i];

  fader.addEventListener('mouseover', function(){
    this.style.color='white';
  });
  fader.addEventListener('mouseout', function(){
    this.style.color='black';
  });
}

// end of init
//}

/*
// For reference: On document ready without jquery:
var readyStateCheckInterval = setInterval(function() {
    if (document.readyState === 'complete') {
        clearInterval(readyStateCheckInterval);
        init();
    }
}, 5);
*/
