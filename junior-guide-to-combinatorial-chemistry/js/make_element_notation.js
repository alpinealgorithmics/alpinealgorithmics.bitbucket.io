$("atom").each(function(index){
    var bold = document.createElement('b');
    bold.innerHTML = ''

    var integer_mass = this.getAttribute('mass');
    if (integer_mass)
	bold.innerHTML = '<sup>' + integer_mass + '</sup>'
    bold.innerHTML += this.innerHTML;
    var charge = this.getAttribute('charge');
    if (charge)
	bold.innerHTML += '<sup>' + charge + '</sup>'

    this.parentNode.replaceChild(bold, this);
});

$("compound").each(function(index){
    var formula = this.innerHTML;
    // ignore last "" due to null terminator
    var elements_and_numbers = formula.split(/([0-9]+)/).slice(0,-1)

    var bold = document.createElement('b');
    bold.innerHTML = ''
    for (var i=0; i<elements_and_numbers.length; i+=2) {
	bold.innerHTML += elements_and_numbers[i];
	if (elements_and_numbers[i+1] > 1)
	    bold.innerHTML += '<sub>' + elements_and_numbers[i+1] + '</sub>';
    }

    this.parentNode.replaceChild(bold, this);
});
