def factorial(n):
  prod = 1
  # loop over 1, 2, ... n < n+1:
  for i in range(1, n+1):
    # prod = prod*i
    prod *= i
  # prod now contains
  # 1 * 2 * 3 * ... * n
  return prod

print( factorial(5) )
