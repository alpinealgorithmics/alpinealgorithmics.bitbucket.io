def factorial(n):
  # base case: we know that 1! = 1
  if n == 1:
    return 1
  # recurse:
  return n * factorial(n-1)

def binomial(x, y):
  return factorial(x+y) // ( factorial(x)*factorial(y) )

print( binomial(2, 3) )
