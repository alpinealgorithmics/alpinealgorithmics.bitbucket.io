seconds_per_minute = 60

total_seconds = 0
# fixme: add another loop!
for second in range(seconds_per_minute):
  # a += b is a shorthand for
  # a = a + b
  # so total_seconds += 1 is shorthand for
  # total_seconds = total_seconds + 1
  # which means it adds 1 to total_seconds
  total_seconds += 1

# total seconds in one hour:
print(total_seconds)
