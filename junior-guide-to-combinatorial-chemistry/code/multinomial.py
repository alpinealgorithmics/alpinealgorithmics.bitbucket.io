def factorial(n):
  # base case: we know that 1! = 1
  if n == 1:
    return 1
  # recurse:
  return n * factorial(n-1)

def multinomial(counts):
  n = sum(counts)
  result = factorial(n)
  for val in counts:
    result = result // factorial(val)
  return result

print( multinomial([2, 3]) )
print( multinomial([2, 3, 2]) )
