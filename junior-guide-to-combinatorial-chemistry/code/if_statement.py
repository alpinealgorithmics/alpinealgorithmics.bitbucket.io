# the single =
# says, set name to 'Bruce Wayne'
name = 'Bruce Wayne'

# the double ==
# asks, "are these the same?"
if name == 'Bruce Wayne':
  print('Hello, Batman!')
else:
  # this will be called if name is not 'Bruce Wayne'
  # i.e., if name != 'Bruce Wayne'
  print('Move along, citizen.')
