def merged(sorted_list_a, sorted_list_b):
  # merge the sorted lists:
  result = []
  a_index = 0
  b_index = 0

  # while indices are in bounds:
  while a_index < len(sorted_list_a) and b_index < len(sorted_list_b):
    a_value = sorted_list_a[a_index]
    b_value = sorted_list_b[b_index]
    if a_value < b_value:
      # first half comes next
      result.append(a_value)
      a_index += 1
    else:
      result.append(b_value)
      b_index += 1
  
  # if one list got to the end first, add the remaining elements from
  # the other list:
  result.extend(sorted_list_a[a_index:])
  result.extend(sorted_list_b[b_index:])
  return result

def sorted_list(some_list):
  n = len(some_list)
  if n == 1:
    # any list with only one item is already sorted!
    return some_list
  
  first_half = some_list[:n//2]
  second_half = some_list[n//2:]
  
  # sort first and second halves!
  sorted_first_half = fixme(first_half)
  sorted_second_half = fixme(second_half)
  
  return merged(sorted_first_half, sorted_second_half)
  
x = [3,5,7,1,6,8]
print(sorted_list(x))
