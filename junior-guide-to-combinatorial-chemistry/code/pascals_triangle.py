def print_layer(layer, max_depth):
  # indent:
  depth = len(layer)
  print(' '*(8*(max_depth-depth)//2), end='')

  for x in layer:
    # print each integer with a size of 8 characters:
    print(f'{x:8d}', end='')
  print()

def print_pascals_triangle(max_depth):
  layer = [1]
  for depth in range(max_depth):
    print_layer(layer, max_depth)

    new_layer = []
  
    new_layer.append(layer[0])
    for i in range(len(layer)-1):
      val = layer[i]
      next_val = layer[i+1]
      new_layer.append(val + next_val)
    new_layer.append(layer[-1])
  
    layer = new_layer
  
print_pascals_triangle(max_depth=15)
