def term(x, i):
  return x**i

def approx_series(x, num_terms):
  return sum([ term(x,i) for i in range(num_terms) ])

def exact_series(x):
  return 1./(1-x)

