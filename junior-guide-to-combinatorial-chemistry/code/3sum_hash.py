import random

n = 1000

values = { random.randint(0, 100000) for i in range(n) }
for a in values:
  for b in values:
    if a+b in values:
      print('3SUM found', a, b, a+b)
