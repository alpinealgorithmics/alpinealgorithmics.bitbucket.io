total = 0
for i in range(10):
  # a += b is a shorthand for
  # a = a + b
  # so total += 1 is shorthand for
  # total = total + 1
  # which means it adds 1 to total
  total += 1

print(total)
