# fetch a list of element masses from the internet:
import urllib.request
print('DOWNLOADING...')
file = urllib.request.urlopen('https://alpinealgorithmics.bitbucket.io/junior-guide-to-combinatorial-chemistry/code/elements.txt')

symbol_to_mass = {}
for line in file.readlines():
  symbol,atomic_mass,name = line.split()

  # be flexible with different versions of urllib:
  if type(symbol) is bytes:
    symbol = symbol.decode('UTF-8')
  symbol_to_mass[symbol] = float(atomic_mass)

# use \n to print an extra newline
print('SUCCESSFULLY DOWNLOADED ELEMENTAL MASSES\n')

print('Atomic mass of hydrogen is', symbol_to_mass['H'])

# read in the chemical formula as a dictionary of atomic symbols
# to number of times the element occurs in the compound
def estimate_atomic_mass(symbol_to_count):
  mass = 0.0
  for symbol,count in symbol_to_count.items():
    atomic_mass = symbol_to_mass[symbol]
    mass += count*atomic_mass
  return mass

# water is H2O = 2 Hydrogen + 1 Oxygen
print('Molecular mass of water is roughly', estimate_atomic_mass({'H':2, 'O':1}))
