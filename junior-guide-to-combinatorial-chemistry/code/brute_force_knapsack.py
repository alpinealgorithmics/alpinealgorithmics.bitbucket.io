import itertools

color_to_mass_and_value = {'R':(2,3), 'O':(5,6), 'Y':(7,8), 'G':(10,4), 'Bl':(11,9), 'Br':(13,2)}

import itertools

color_to_mass = {'R':2, 'O':5, 'Y':7, 'G':10, 'Bl':11, 'Br':13}

def total_mass(comb):
  return sum([ count*color_to_mass_and_value[color][0] for color,count in comb ])

def total_value(comb):
  return sum([ count*color_to_mass_and_value[color][1] for color,count in comb ])

def combinations_helper(obj_and_count_s, i, k):
  if k > sum([b for a,b in obj_and_count_s]):
    return
  if k<=0:
    yield( () )
    return
  if i < len(obj_and_count_s):
    obj,count = obj_and_count_s[i]
    for j in range(min(k,count)+1):
      if j>0:
        lhs = ( (obj,j), )
      else:
        lhs = ()
      for rhs in combinations_helper(obj_and_count_s, i+1, k-j):
        yield( lhs + rhs )

def combinations(multiset, k):
  return combinations_helper(list(multiset.items()), 0, k)

def solve_subset_sum(goal_mass):
  ingredients = {}
  for color,(mass,value) in color_to_mass_and_value.items():
    max_contributions_of_color = goal_mass // mass
    ingredients[color] = max_contributions_of_color

  # allow 0, 1, 2, ... num_available < num_available+1 to be chosen:
  best_value = -1
  for number_to_use in range(sum(color_to_mass.values())//min(color_to_mass.values())+1):
    print('choosing', number_to_use, 'from the ingredients')

    for comb in combinations(ingredients, number_to_use):
      mass = total_mass(comb)
      value = total_value(comb)
      if mass == goal_mass and value > best_value:
        best = comb
        best_value = value
        print('new best:', comb, mass, value)
    print()

  print('best', best)

solve_subset_sum(25)
