# Use functions and loops to print these lyrics:

#### On the first day of classes, my teacher gave to me
#### A python script written for me

#### On the second day of classes, my teacher gave to me
#### Two nested loops
#### And a python script written for me

#### On the third day of classes, my teacher gave to me
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the fourth day of classes, my teacher gave to me
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the fifth day of classes, my teacher gave to me
#### Five scatter plots
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the sixth day of classes, my teacher gave to me
#### Six graphs a graphing
#### Five scatter plots
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the seventh day of classes, my teacher gave to me
#### Seven dictionaries
#### Six graphs a graphing
#### Five scatter plots
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the eighth day of classes, my teacher gave to me
#### Eight classy classes
#### Seven dictionaries
#### Six graphs a graphing
#### Five scatter plots
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the ninth day of classes, my teacher gave to me
#### Nine lists a listing
#### Eight classy classes
#### Seven dictionaries
#### Six graphs a graphing
#### Five scatter plots
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the tenth day of classes, my teacher gave to me
#### 10 Long integers
#### Nine lists a listing
#### Eight classy classes
#### Seven dictionaries
#### Six graphs a graphing
#### Five scatter plots
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the eleventh day of classes, my teacher gave to me
#### 11 Hot-hot heatmaps
#### 10 Long integers
#### Nine lists a listing
#### Eight classy classes
#### Seven dictionaries
#### Six graphs a graphing
#### Five scatter plots
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me

#### On the twelfth day of classes, my teacher gave to me
#### 12 decorators
#### 11 Hot-hot heatmaps
#### 10 Long integers
#### Nine lists a listing
#### Eight classy classes
#### Seven dictionaries
#### Six graphs a graphing
#### Five scatter plots
#### Four function calls
#### Three if statements
#### Two nested loops
#### And a python script written for me
