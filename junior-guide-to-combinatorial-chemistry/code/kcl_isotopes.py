import itertools

k_mass_and_frequency_s = [ (38.963706487,0.932581), (39.96399817,1.17e-4), (40.961825258,0.067302) ]
cl_mass_and_frequency_s = [ (34.96885269,0.7576), (36.96590258,0.2424) ]

for (k_mass, k_freq), (cl_mass, cl_freq) in itertools.product(k_mass_and_frequency_s, cl_mass_and_frequency_s):
  print(k_mass+cl_mass, k_freq*cl_freq)
