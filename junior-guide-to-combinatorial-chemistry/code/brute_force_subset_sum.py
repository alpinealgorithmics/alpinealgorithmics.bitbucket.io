import itertools

color_to_mass = {'R':2, 'O':5, 'Y':7, 'G':10, 'Bl':11, 'Br':13}

def total_mass(comb):
  return sum([ count*color_to_mass[color] for color,count in comb ])

def combinations_helper(obj_and_count_s, i, k):
  if k > sum([b for a,b in obj_and_count_s]):
    return
  if k<=0:
    yield( () )
    return
  if i < len(obj_and_count_s):
    obj,count = obj_and_count_s[i]
    for j in range(min(k,count)+1):
      if j>0:
        lhs = ( (obj,j), )
      else:
        lhs = ()
      for rhs in combinations_helper(obj_and_count_s, i+1, k-j):
        yield( lhs + rhs )

def combinations(multiset, k):
  return combinations_helper(list(multiset.items()), 0, k)

def solve_subset_sum(goal_mass):
  ingredients = {}
  for color,mass in color_to_mass.items():
    max_contributions_of_color = goal_mass // mass
    ingredients[color] = max_contributions_of_color

  # allow 0, 1, 2, ... num_available < num_available+1 to be chosen:
  for number_to_use in range(sum(color_to_mass.values())//min(color_to_mass.values())+1):
    print('choosing', number_to_use, 'from the ingredients')

    for comb in combinations(ingredients, number_to_use):
      tot = total_mass(comb)
      if tot == goal_mass:
        print(comb, tot)
        ## we could stop here if one solution is enough:
        #break
    print()

solve_subset_sum(25)
