# [x] * 5 = [x, x, x, x, x]
available = {'orange':4, 'brown':2, 'blue':3}
print(available)
print()

def combinations_helper(obj_and_count_s, i, k):
  if k <= 0:
    return [()]
  if k>sum([b for a,b in obj_and_count_s]) or i >= len(obj_and_count_s):
    return []
  obj,count = obj_and_count_s[i]
  result = []
  for j in range(min(k,count)+1):
    if j>0:
      lhs = ( (obj,j), )
    else:
      lhs = ()
    for  rhs in combinations_helper(obj_and_count_s, i+1, k-j):
      result.append( lhs + rhs )
  return result

def combinations(multiset, k):
  return combinations_helper(list(multiset.items()), 0, k)

num_available = len(available)
# allow 0, 1, 2, ... num_available < num_available+1 to be chosen:
for number_to_use in range(num_available+1):
  print('choosing', number_to_use, 'from the available')
  # find all ways to select number_to_use from available.
  # these ways should be unique (when ignoring what order the M&Ms are in)
  for comb in combinations(available,  number_to_use):
    print(comb)
  print()
