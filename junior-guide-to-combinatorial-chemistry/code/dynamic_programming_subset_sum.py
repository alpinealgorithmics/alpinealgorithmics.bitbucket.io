color_to_mass = {'R':2, 'O':5, 'Y':7, 'G':10, 'Bl':11, 'Br':13}

def total_mass(comb):
  return sum([ count*color_to_mass[color] for color,count in comb ])

def merge(dict_a, dict_b, goal_mass):
  result = {}
  for ka,va in dict_a.items():
    for kb,vb in dict_b.items():
      mass = va+vb
      if mass <= goal_mass:
        result[ka+kb] = mass
  return result

def get_contributions_from_color_and_mass(color, mass, goal_mass):
  max_contributions_of_color = goal_mass // mass
  layer_from_color = { ((color,i),) : i*mass for i in range(max_contributions_of_color+1)}
  return layer_from_color

def solve_subset_sum(goal_mass):
  name_to_total_mass = { (('',0),) : 0 }
  for color,mass in color_to_mass.items():
    layer_from_color = get_contributions_from_color_and_mass(color, mass, goal_mass)
    name_to_total_mass = merge(name_to_total_mass, layer_from_color, goal_mass)

  for name,total_mass in name_to_total_mass.items():
    if total_mass == goal_mass:
      print(name, total_mass)
  
solve_subset_sum(25)
