# i%2 gives the remainder when i is divided in half
# so if i%2==0, then the remainder is 0, i.e., i is an even number
values_divisible_by_2 = [ i for i in range(10) if i%2 == 0 ]

print(values_divisible_by_2)
print(sum(values_divisible_by_2))
