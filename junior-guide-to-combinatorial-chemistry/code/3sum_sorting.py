import bisect
import random

n = 1000

sorted_list = sorted([ random.randint(0, 100000) for i in range(n) ])
for a in sorted_list:
  for b in sorted_list:
    c = a+b
    
    index = bisect.bisect_left(sorted_list, c)
    if index >= 0 and index < n:
      if sorted_list[index] == c:
        print('3SUM found', a, b, c)
