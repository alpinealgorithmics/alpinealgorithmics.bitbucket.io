color_to_mass = {'R':2, 'O':5, 'Y':7, 'G':10, 'Bl':11, 'Br':13}

def merge(dict_a, dict_b, goal_mass):
  result = {}
  for ka,va in dict_a.items():
    for kb,vb in dict_b.items():
      mass = ka+kb
      if mass <= goal_mass:
        result[ka+kb] = va+vb
  return result

def get_contributions_from_color_and_mass(color, mass, goal_mass):
  max_contributions_of_color = goal_mass // mass
  layer_from_color = { i*mass : ((color,i),) for i in range(max_contributions_of_color+1)}
  return layer_from_color

def solve_subset_sum(goal_mass):
  mass_to_name = { 0 : (('',0),) }
  for color,mass in color_to_mass.items():
    layer_from_color = get_contributions_from_color_and_mass(color, mass, goal_mass)
    mass_to_name = merge(mass_to_name, layer_from_color, goal_mass)

  if goal_mass in mass_to_name:
    print( mass_to_name[goal_mass] )
    # this implementation finds only one solution (if one exists).
    # to find multiple solutions, mass_to_name should be int:list(...),
    # and merge should be updated correspondingly.

solve_subset_sum(25)
