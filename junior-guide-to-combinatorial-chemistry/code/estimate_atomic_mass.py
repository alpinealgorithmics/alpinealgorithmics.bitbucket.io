# Note: does not include relativity, which means this estimate is not exact.
# The difference between the sum of the pieces and the true mass is the "mass defect."
def estimate_atomic_mass(atomic_number, num_neutrons, num_electrons):
  # masses are in amu
  proton_mass =   1.0072765
  neutron_mass =  1.0085665
  electron_mass = 0.00054858

  num_protons = atomic_number
  return num_protons*proton_mass + num_neutrons*neutron_mass + num_electrons*electron_mass

# carbon, 6 neutrons, neutral charge:
carbon_atomic_mass = 12.011
print( estimate_atomic_mass(6, 6, 6), 'vs true value', carbon_atomic_mass )

# helium, 2 neutrons, one extra electron:
helium_atomic_mass = 4.003
print( estimate_atomic_mass(2, 2, 3), 'vs true value', helium_atomic_mass )

# oxygen, 8 neutrons, neutral charge:
oxygen_atomic_mass = 15.999
print( estimate_atomic_mass(8, 8, 8), 'vs true value', oxygen_atomic_mass )

# lithium, 4 neutrons, missing one electron:
lithium_atomic_mass = 6.941
print( estimate_atomic_mass(3, 4, 2), 'vs true value', lithium_atomic_mass )
