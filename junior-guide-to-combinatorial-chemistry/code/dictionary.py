name_to_age = {'John':10, 'Mary':7, 'Anthony':13}

# add another person to name_to_age
name_to_age['Matthew'] = 15

# lookup john's age
print( name_to_age['John'] )
