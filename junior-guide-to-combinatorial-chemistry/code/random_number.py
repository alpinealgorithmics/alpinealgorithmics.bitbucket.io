import random

num_students = 15
vals_used = set()
for i in range(num_students):
  val = random.randint(10, 100)
  # disallow duplicates:
  while val in vals_used:
    val = random.randint(10, 100)
  vals_used.add(val)
  print(val)
